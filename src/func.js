const getSum = (str1, str2) => {
  if(typeof str1 != "string" || typeof str2 != "string") return false;

  if(str1.length <= 1) return str2;
  else if(str2.length <= 1) return str1;

  if(parseInt(str1) != str1 || parseInt(str2) != str2) return false;

  let res = [];

  for(let i = 0; i < str1.length; i++){
    res.push(parseInt(str1[i]) + parseInt(str2[i]));
  }

  return res.join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

  let pcount = 0, ccount = 0;
  
  for(let post of listOfPosts){
    if(post.author == authorName) pcount++;

    if(post.hasOwnProperty("comments")){

      for(let comment of post.comments){
        if(comment.author == authorName) ccount++;
      }

    }
  }

  return "Post:" + pcount + ",comments:" + ccount;
};

const tickets = (people)=> {
  let account = 0;
  for(let client of people){
    let dffrnc = parseInt(client) - 25;
    if(dffrnc > account) return "NO";
    else account -= dffrnc;
    account += 25;
  }

  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
